#!/usr/bin/env bash
set -ex

# Compile:
javac Server1.java
javac Server2.java

# Connect docker to minikube docker daemon:
eval $(minikube docker-env)

# Create containers:
docker build -t server1:0.0.1 -f Dockerfile1 .
docker build -t server2:0.0.1 -f Dockerfile2 .

# Run container
docker run -p 8500:8500 server1:0.0.1
docker run -p 8500:8500 server2:0.0.1
