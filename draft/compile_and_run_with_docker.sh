#!/usr/bin/env bash
set -ex

# Compile:
javac Server.java

# Connect docker to minikube docker daemon:
eval $(minikube docker-env)

# Create container:
docker build -t myjava:0.0.1 .

# Run container
docker run -p 8500:8500 myjava:0.0.1
