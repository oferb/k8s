#!/usr/bin/env bash
set -ex

# Compile:
javac Server.java

# Connect docker to minikube docker daemon:
eval $(minikube docker-env)

# Create container:
docker build -t myjava:0.0.1 .

# Run container
kubectl run myjavacontainer --image=myjava:0.0.1 --image-pull-policy=Never --port=8500 --expose

# Open the port (TODO: needs fixing to not rely on port):
#kubectl port-forward myjavacontainer-844c5d9f7-qzw6f 8500:8500

# Debugging:
# kubectl get pods
# kubectl logs myjavacontainer-bc64f45fc-5j84p