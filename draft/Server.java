import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server {

    public static void main(String[] args) throws Exception {
        System.out.println("Start main");
        // Thread.sleep(20000000);
        HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
        System.out.println("Step1");
        HttpContext context = server.createContext("/");
        System.out.println("Step2");
        context.setHandler(Server::handleRequest);
        System.out.println("Step3");
        server.start();
        System.out.println("Step4");
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {
        System.out.println("Start handleRequest");
        String response = "Hi there!";
        exchange.sendResponseHeaders(200, response.getBytes().length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
        System.out.println("End handleRequest");
    }
}